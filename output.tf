output "bucket_origin_name" {
  value = aws_s3_bucket.origin.id
}

output "bucket_log_name" {
  value = aws_s3_bucket.log.id
}

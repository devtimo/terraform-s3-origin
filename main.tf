# spa/hosting_and_cdnlog_buckets

resource "aws_s3_bucket" "origin" {
  region        = var.region
  bucket        = var.bucket
  acl           = "private"
  force_destroy = false

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  versioning {
    enabled = true
  }

  # acceleration_status = "Enabled"
}

resource "aws_s3_bucket" "log" {
  region        = var.region
  bucket        = "${var.bucket}-log"
  acl           = "log-delivery-write"
  force_destroy = false
}

resource "aws_s3_bucket_policy" "origin" {
  bucket = aws_s3_bucket.origin.id
  policy = data.aws_iam_policy_document.origin.json
}

data "aws_iam_policy_document" "origin" {
  statement {
    sid = "PublicReadGetObject"
    actions = [
      "s3:GetObject"
    ]

    resources = ["${aws_s3_bucket.origin.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}